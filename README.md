# Device Marketplace

This simple web-application lets you create, update and delete insertions about electronic devices such as laptops and smartphones.
You can create your own account and start to publish your insertions, or search for a specific device and buy it.

## Installing the application

to install the application, simply clone the repository on your machine.


## Build and execute JAR file (Maven)

You can run the application by using the following command:

```bash
./mvnw spring-boot:run
```
Alternatively, you can build the JAR file with the following command:

```bash 
./mvnw clean package
```

Then, run the JAR file as follows:

```bash 
java -jar target/Assignment3-0.0.1-SNAPSHOT.jar
```
