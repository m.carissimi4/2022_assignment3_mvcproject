package com.assignment3.Assignment3.controller;

import com.assignment3.Assignment3.models.*;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.cassandra.CassandraProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Controller
public class WebController {
    private ArrayList<Device> devices = new ArrayList<>();
    private HashMap<String, Account> accounts = new HashMap<String, Account>();
    private Account activeAccount = null;

    @GetMapping("/")
    public ModelAndView index(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("devices", devices);
        modelAndView.addObject("activeAccount", activeAccount);
        modelAndView.setViewName("index");
        return modelAndView;
    }


    @GetMapping("/search")
    public ModelAndView search(@RequestParam(name = "query", required = false, defaultValue = "") String query, Model model) {
        ModelAndView modelAndView = new ModelAndView();

        ArrayList<Device> devicesQueryResults = new ArrayList<>();
        ArrayList<Account> usersQueryResults = new ArrayList<>();

        if(!query.equals("")) {
            for (Device device : devices) {
                if(device.getName().contains(query) || device.getVendor().equals(query))
                    devicesQueryResults.add(device);
            }

            for (Map.Entry<String, Account> entry : accounts.entrySet()) {
                if (entry.getKey().contains(query))
                    usersQueryResults.add(entry.getValue());
            }
        }

        modelAndView.addObject("devicesQueryResults", devicesQueryResults);
        modelAndView.addObject("usersQueryResults", usersQueryResults);
        modelAndView.setViewName("search");
        modelAndView.addObject("activeAccount", activeAccount);
        return modelAndView;
    }
    
    @GetMapping("/follow/{accountToBeFollowed}")
    public ModelAndView searchFollow(@PathVariable("accountToBeFollowed") String accountToBeFollowed,
                                     @RequestParam(required = false) String referrer, Model model) {
        activeAccount.followAccount(accounts.get(accountToBeFollowed));
        if(referrer != null){
            if(referrer.equals("/search")){
                return search("", model);
            }
            if(referrer.equals("/profile")){
                return profile("", "");
            }
        }
        return index();
    }

    @GetMapping("/unfollow/{accountToBeUnfollowed}")
    public ModelAndView searchUnfollow(@PathVariable("accountToBeUnfollowed") String accountToBeUnfollowed,
                                       @RequestParam(required = false) String referrer, Model model) {
        activeAccount.unfollowAccount(accountToBeUnfollowed);
        if(referrer != null){
            if(referrer.equals("/search")){
                return search("", model);
            }
            if(referrer.equals("/profile")){
                return profile("", "");
            }
        }
        return index();
    }

    @GetMapping("/addSmartphone")
    public ModelAndView addSmartphone(@RequestParam(name = "smartphoneName", required = false, defaultValue = "") String deviceName,
                                  @RequestParam(name = "smartphoneBrand", required = false, defaultValue = "") String deviceBrand,
                                  @RequestParam(name = "smartphoneReleaseDate", required = false, defaultValue = "") String deviceReleaseDate,
                                  @RequestParam(name = "smartphoneCamera", required = false, defaultValue = "") String deviceCamera,
                                  @RequestParam(name = "smartphonePrice", required = false, defaultValue = "0") double devicePrice,
                                  Model model) {
        ModelAndView modelAndView = new ModelAndView();
        if(!deviceName.equals(""))
            devices.add(new Smartphone(deviceName, deviceBrand, deviceReleaseDate, deviceCamera, devicePrice, activeAccount.getUsername()));

        modelAndView.addObject("devices", devices);
        modelAndView.addObject("activeAccount", activeAccount);
        modelAndView.setViewName("addSmartphone");

        return modelAndView;
    }

    @GetMapping("/addLaptop")
    public ModelAndView addLaptop(@RequestParam(name = "laptopName", required = false, defaultValue = "") String deviceName,
                                  @RequestParam(name = "laptopBrand", required = false, defaultValue = "") String deviceBrand,
                                  @RequestParam(name = "laptopReleaseDate", required = false, defaultValue = "") String deviceReleaseDate,
                                  @RequestParam(name = "laptopKeyboard", required = false, defaultValue = "") String laptopKeyboard,
                                  @RequestParam(name = "laptopPrice", required = false, defaultValue = "0") double laptopPrice,
                                  Model model) {
        ModelAndView modelAndView = new ModelAndView();
        if(!deviceName.equals(""))
            devices.add(new Laptop(deviceName, deviceBrand, deviceReleaseDate, laptopKeyboard, laptopPrice, activeAccount.getUsername()));

        modelAndView.addObject("devices", devices);
        modelAndView.addObject("activeAccount", activeAccount);
        modelAndView.setViewName("addLaptop");

        return modelAndView;
    }


    @GetMapping("/signUp")
    public ModelAndView signUp(@RequestParam(name = "name", required = false, defaultValue = "") String name,
                               @RequestParam(name = "surname", required = false, defaultValue = "") String surname,
                               @RequestParam(name = "username", required = false, defaultValue = "") String username,
                               @RequestParam(name = "password", required = false, defaultValue = "") String password,
                               @RequestParam(name = "repeatPassword", required = false, defaultValue = "") String repeatPassword,
                               @RequestParam(name = "accountType", required = false, defaultValue = "") String accountType) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("signUp");

        if(!name.equals("")) {
            if(!password.equals(repeatPassword)) {
                modelAndView.addObject("outcomeMessage", "Password mismatch");
                return modelAndView;
            }

            Account account;
            if(accountType.equals(""))
                account = new Customer(name, surname, username, password);
            else
                account = new Vendor(name, surname, username, password);

            if(accounts.containsValue(account)) {
                modelAndView.addObject("outcomeMessage", "Account already exists");
                return modelAndView;
            }

            accounts.put(username, account);
            activeAccount = account;
            modelAndView.addObject("outcomeMessage", "Account successfully created!");
        } else
            modelAndView.addObject("outcomeMessage", "");


        modelAndView.addObject("activeAccount", activeAccount);
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView login (@RequestParam(name = "username", required = false, defaultValue = "") String username,
                                @RequestParam(name = "password", required = false, defaultValue = "") String password) {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");

        if(!username.equals("") && !password.equals("")) {
            if(accounts.containsKey(username) && accounts.get(username).getPassword().equals(password)) {
                activeAccount = accounts.get(username);
                modelAndView.addObject("outcomeMessage", "Successfully logged in!");
            }
            else
                modelAndView.addObject("outcomeMessage", "Wrong credentials");

        }

        modelAndView.addObject("activeAccount", activeAccount);
        return modelAndView;
    }

    @GetMapping("/profile")
    public ModelAndView profile(@RequestParam(name = "newPsw", required = false, defaultValue = "") String newPsw,
                                @RequestParam(name = "newPswConfirm", required = false, defaultValue = "") String newPswConfirm) {
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<Account> followedUsers = new ArrayList<>();
        boolean isPswChanged = false;
        boolean pswError = false;

        if(!newPsw.equals("") && newPsw.equals(newPswConfirm)){
            activeAccount.setPassword(newPsw);
            isPswChanged = true;
        }
        else{
            if(!newPsw.equals("")) {
                pswError = true;
            }
        }

        for (Map.Entry<String, Account> entry : activeAccount.getFollowed().entrySet()) {
            followedUsers.add(entry.getValue());
        }

        ArrayList<Object> userDevices = new ArrayList<>();
        for (Device device: devices) {
            if(device.getVendor().equals(activeAccount.getUsername())){
                userDevices.add(device);
            }
        }

        modelAndView.addObject("isPersonalProfile", true);
        modelAndView.addObject("profileAccount", activeAccount);
        modelAndView.addObject("followedUsers", followedUsers);
        modelAndView.addObject("devices", userDevices);
        modelAndView.addObject("pswChanged", isPswChanged);
        modelAndView.addObject("pswError", pswError);

        modelAndView.setViewName("/profile");
        return modelAndView;
    }

    @GetMapping("/profile/{userProfile}")
    public ModelAndView publicProfile(@PathVariable("userProfile") String userProfile, Model model){

        Account profileAccount;
        boolean isActiveAccount = false;
        ModelAndView modelAndView = new ModelAndView();
        ArrayList<Account> followedUsers = new ArrayList<>();

        if(activeAccount != null && activeAccount.getUsername().equals(userProfile)){
            profileAccount = activeAccount;
            isActiveAccount = true;
        }
        else{
            profileAccount = accounts.get(userProfile);
        }

        for (Map.Entry<String, Account> entry : profileAccount.getFollowed().entrySet()) {
            followedUsers.add(entry.getValue());
        }

        ArrayList<Object> userDevices = new ArrayList<>();
        for (Device device: devices) {
            if(device.getVendor().equals(profileAccount.getUsername())){
                userDevices.add(device);
            }
        }

        modelAndView.addObject("isPersonalProfile", isActiveAccount);
        modelAndView.addObject("profileAccount", profileAccount);
        modelAndView.addObject("followedUsers", followedUsers);
        modelAndView.addObject("devices", userDevices);
        modelAndView.setViewName("profile");

        return modelAndView;
    }

    @GetMapping("/delete/{deviceToBeDeleted}")
    public ModelAndView deleteDevice(@PathVariable("deviceToBeDeleted") String deviceToBeDeleted, Model model){

        for(Device device : devices){
            if(device.getName().equals(deviceToBeDeleted)){
                devices.remove(device);
                return profile("", "");
            }
        }

        return profile("", "");

    }

    @GetMapping("/logout")
    public ModelAndView logout() {

        ModelAndView modelAndView = new ModelAndView();
        activeAccount = null;
        modelAndView.addObject("activeAccount", null);
        modelAndView.setViewName("/logout");

        return modelAndView;
    }

    @GetMapping("/buy")
    public ModelAndView buy(@RequestParam(name = "deviceToBeBought", required = false, defaultValue = "") String deviceToBeBoughtName,
                            @RequestParam(required = false) String referrer, Model model) {
        int deviceIndex = -1;
        int loopIndex = 0;
        boolean found = false;

        while(!found && loopIndex < devices.size()) {
            if(devices.get(loopIndex).getName().equals(deviceToBeBoughtName)){
                deviceIndex = loopIndex;
                found = true;
            }
            loopIndex++;
        }


        if(activeAccount != null) {
            activeAccount.getBoughtDevices().add(devices.get(deviceIndex));
            devices.remove(deviceIndex);
        }

        if(referrer != null){
            if(referrer.equals("/search")){
                return search("", model);
            }
        }

        return index();
    }

}
