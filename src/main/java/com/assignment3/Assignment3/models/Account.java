package com.assignment3.Assignment3.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Objects;

public abstract class Account {
    private String name;
    private String surname;
    private String username;
    private String password;
    private HashMap<String, Account> followed;
    private ArrayList<Device> boughtDevices;

    public Account(String name, String surname, String username, String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        followed = new HashMap<>();
        boughtDevices = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public HashMap<String, Account> getFollowed() {
        return followed;
    }

    public ArrayList<Device> getBoughtDevices() {
        return boughtDevices;
    }

    public void followAccount (Account account) {
        if(!followed.containsKey(account.getUsername())) {
            System.out.println("Size before following " + followed.size());
            followed.put(account.getUsername(), account);
            System.out.println("Size after following " + followed.size());
        }
    }

    public void unfollowAccount(String username) {
        System.out.println("Size before unfollowing " + followed.size());
        followed.remove(username);
        System.out.println("Size after unfollowing " + followed.size());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Account account = (Account) o;
        return Objects.equals(getName(), account.getName()) && Objects.equals(getSurname(), account.getSurname()) && Objects.equals(getUsername(), account.getUsername());
    }
}
