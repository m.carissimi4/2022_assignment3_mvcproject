package com.assignment3.Assignment3.models;

public abstract class Device {
    private String name;
    private String brand;
    private String releaseDate;
    private double price;
    private String vendor;

    public Device(String name, String brand, String releaseDate, double price, String vendor) {
        this.name = name;
        this.brand = brand;
        this.releaseDate = releaseDate;
        this.price = price;
        this.vendor = vendor;
    }

    public Device() {

    }

    @Override
    public String toString() {
        return "Device{" +
                "name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public double getPrice() {
        return price;
    }

    public String getVendor() {
        return vendor;
    }
}
