package com.assignment3.Assignment3.models;

public class Laptop extends Device {
    private String keyboardLayout;

    public Laptop(){
        super();
    }

    public Laptop(String name, String brand, String releaseDate, String keyboardFormat, double price, String vendor) {
        super(name, brand, releaseDate, price, vendor);
        this.keyboardLayout = keyboardFormat;
    }

    public String getKeyboardLayout() {
        return keyboardLayout;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "keyboardFormat='" + keyboardLayout + '\'' +
                "} " + super.toString();
    }
}
