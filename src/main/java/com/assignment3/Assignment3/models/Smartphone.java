package com.assignment3.Assignment3.models;

public class Smartphone extends Device{

    private String camera;


    public Smartphone(String name, String brand, String releaseDate, String camera, double price, String vendor) {
        super(name, brand, releaseDate, price, vendor);
        this.camera = camera;
    }

    public String getCamera() {
        return camera;
    }

    @Override
    public String toString() {
        return "Smartphone{" +
                "camera='" + camera + '\'' +
                "} " + super.toString();
    }
}
